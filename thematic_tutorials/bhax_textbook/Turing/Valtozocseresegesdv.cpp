#include <iostream>

int main()
{
    int a = 1, b = 2;
    int c;
    std::cout << "a= "<<a<<" b= "<<b<<'\n';
    c = a;
    a = b;
    b = c;
    std::cout << "a= " << a << " b= " << b << '\n';
}
