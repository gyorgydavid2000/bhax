#include <iostream>
#include <stdlib.h>
#include <ctime>
#include <vector>
#include <cstdlib>

using namespace std;

int main()
{
    vector<int> nyertes, jatekos, jatekos2, musorvezeto;
    int valtoztatott=0,nemvaltoztatott=0,a,n = 10000000;
    srand(time(0));
    for (int i = 0; i < n; i++)
    {
        nyertes.push_back(rand()%3+1);
        jatekos.push_back(rand() % 3 + 1);
    }
    for (int i = 0; i < n; i++)
    {
        if (nyertes[i]==jatekos[i])
        {
            a = rand() % 3 + 1;
            while (a==jatekos[i])
            {
                a = rand() % 3 + 1;
            }
            musorvezeto.push_back(a);
        }
        else
        {
            a = rand() % 3 + 1;
            while (a == jatekos[i] || a==nyertes[i])
            {
                a = rand() % 3 + 1;
            }
            musorvezeto.push_back(a);
        }
        if (musorvezeto[i]!=1 && jatekos[i]!=1)
        {
            jatekos2.push_back(1);
        }
        else if (musorvezeto[i] != 2 && jatekos[i] != 2)
        {
            jatekos2.push_back(2);
        }
        else {
            jatekos2.push_back(3);
        }
        if (jatekos[i]==nyertes[i])
        {
            nemvaltoztatott++;
        }
        if (jatekos2[i] == nyertes[i])
        {
            valtoztatott++;
        }
    }    
    cout << "Valtoztat�s n�lk�li nyer�sek sz�ma: " << nemvaltoztatott << '\n';
    cout << "Valtoztat�ssal nyert j�t�kok sz�ma: " << valtoztatott << '\n';
    return 0;
}