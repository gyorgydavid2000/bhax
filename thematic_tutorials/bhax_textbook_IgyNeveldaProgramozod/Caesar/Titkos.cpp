#include <iostream>
#include <vector>

using namespace std;

void exor(string kulcs, vector<char> &szoveg){
   for(int i=0; i<szoveg.size(); i++){
        szoveg[i]=szoveg[i]^kulcs[i%kulcs.size()];
    }
} 

int main(){
   string kulcs;
   vector<char> szoveg; 
   char a;
   cin>>kulcs;
   while(cin.get(a)){
    szoveg.push_back(a);
   }
   exor(kulcs,szoveg);
   for(int i=0; i<szoveg.size(); i++){
        cout<<szoveg[i];
    }
}