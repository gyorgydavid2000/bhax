/*
 * Sejtautomata.java
 *
 * DIGIT 2005, Javat tantok
 * Btfai Norbert, nbatfai@inf.unideb.hu
 *
 */
/**
 * Sejtautomata osztly.
 *
 * @author Btfai Norbert, nbatfai@inf.unideb.hu
 * @version 0.0.1
 */
public class Sejtautomata extends java.awt.Frame implements Runnable {
    /** Egy sejt lehet l */
    public static final boolean L = true;
    /** vagy halott */
    public static final boolean HALOTT = false;
    /** Kt rcsot hasznlunk majd, az egyik a sejttr llapott
     * a t_n, a msik a t_n+1 idpillanatban jellemzi. */
    protected boolean [][][] rcsok = new boolean [2][][];
    /** Valamelyik rcsra mutat, technikai jelleg, hogy ne kelljen a
     * [2][][]-bl az els dimenzit hasznlni, mert vagy az egyikre
     * lltjuk, vagy a msikra. */
    protected boolean [][] rcs;
    /** Megmutatja melyik rcs az aktulis: [rcsIndex][][] */
    protected int rcsIndex = 0;
    /** Pixelben egy cella adatai. */
    protected int cellaSzlessg = 20;
    protected int cellaMagassg = 20;
    /** A sejttr nagysga, azaz hnyszor hny cella van? */
    protected int szlessg = 20;
    protected int magassg = 10;
    /** A sejttr kt egymst kvet t_n s t_n+1 diszkrt idpillanata
     kztti vals id. */  
    protected int vrakozs = 1000;
    // Pillanatfelvtel ksztshez
    private java.awt.Robot robot;
    /** Ksztsnk pillanatfelvtelt? */
    private boolean pillanatfelvtel = false;
    /** A pillanatfelvtelek szmozshoz. */
    private static int pillanatfelvtelSzmll = 0;
    /**
     * Ltrehoz egy <code>Sejtautomata</code> objektumot.
     *
     * @param      szlessg    a sejttr szlessge.
     * @param      magassg     a sejttr szlessge.
     */
    public Sejtautomata(int szlessg, int magassg) {
        this.szlessg = szlessg;
        this.magassg = magassg;
        // A kt rcs elksztse
        rcsok[0] = new boolean[magassg][szlessg];
        rcsok[1] = new boolean[magassg][szlessg];
        rcsIndex = 0;
        rcs = rcsok[rcsIndex];
        // A kiindul rcs minden cellja HALOTT
        for(int i=0; i<rcs.length; ++i)
            for(int j=0; j<rcs[0].length; ++j)
                rcs[i][j] = HALOTT;
        // A kiindul rcsra "llnyeket" helyeznk
        //sikl(rcs, 2, 2);
        siklKilv(rcs, 5, 60);
        // Az ablak bezrsakor kilpnk a programbl.
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent e) {
                setVisible(false);
                System.exit(0);
            }
        });
        // A billentyzetrl rkez esemnyek feldolgozsa
        addKeyListener(new java.awt.event.KeyAdapter() {
            // Az 'k', 'n', 'l', 'g' s 's' gombok lenyomst figyeljk
            public void keyPressed(java.awt.event.KeyEvent e) {
                if(e.getKeyCode() == java.awt.event.KeyEvent.VK_K) {
                    // Felezk a cella mreteit:
                    cellaSzlessg /= 2;
                    cellaMagassg /= 2;
                    setSize(Sejtautomata.this.szlessg*cellaSzlessg,
                            Sejtautomata.this.magassg*cellaMagassg);
                    validate();
                } else if(e.getKeyCode() == java.awt.event.KeyEvent.VK_N) {
                    // Duplzzuk a cella mreteit:
                    cellaSzlessg *= 2;
                    cellaMagassg *= 2;
                    setSize(Sejtautomata.this.szlessg*cellaSzlessg,
                            Sejtautomata.this.magassg*cellaMagassg);
                    validate();
                } else if(e.getKeyCode() == java.awt.event.KeyEvent.VK_S)
                    pillanatfelvtel = !pillanatfelvtel;
                else if(e.getKeyCode() == java.awt.event.KeyEvent.VK_G)
                    vrakozs /= 2;
                else if(e.getKeyCode() == java.awt.event.KeyEvent.VK_L)
                    vrakozs *= 2;
                repaint();
            }
        });
        // Egr kattint esemnyek feldolgozsa:
        addMouseListener(new java.awt.event.MouseAdapter() {
            // Egr kattintssal jelljk ki a nagytand terletet
            // bal fels sarkt vagy ugyancsak egr kattintssal
            // vizsgljuk egy adott pont iterciit:
            public void mousePressed(java.awt.event.MouseEvent m) {
                // Az egrmutat pozcija
                int x = m.getX()/cellaSzlessg;
                int y = m.getY()/cellaMagassg;
                rcsok[rcsIndex][y][x] = !rcsok[rcsIndex][y][x];
                repaint();
            }
        });
        // Egr mozgs esemnyek feldolgozsa:
        addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            // Vonszolssal jelljk ki a ngyzetet:
            public void mouseDragged(java.awt.event.MouseEvent m) {
                int x = m.getX()/cellaSzlessg;
                int y = m.getY()/cellaMagassg;
                rcsok[rcsIndex][y][x] = L;
                repaint();
            }
        });
        // Cellamretek kezdetben
        cellaSzlessg = 10;
        cellaMagassg = 10;
        // Pillanatfelvtel ksztshez:
        try {
            robot = new java.awt.Robot(
                    java.awt.GraphicsEnvironment.
                    getLocalGraphicsEnvironment().
                    getDefaultScreenDevice());
        } catch(java.awt.AWTException e) {
            e.printStackTrace();
        }
        // A program ablaknak adatai:
        setTitle("Sejtautomata");
        setResizable(false);
        setSize(szlessg*cellaSzlessg,
                magassg*cellaMagassg);
        setVisible(true);
        // A sejttr letrekeltse:
        new Thread(this).start();
    }
    /** A sejttr kirajzolsa. */
    public void paint(java.awt.Graphics g) {
        // Az aktulis
        boolean [][] rcs = rcsok[rcsIndex];
        // rcsot rajzoljuk ki:
        for(int i=0; i<rcs.length; ++i) { // vgig lpked a sorokon
            for(int j=0; j<rcs[0].length; ++j) { // s az oszlopok
                // Sejt cella kirajzolsa
                if(rcs[i][j] == L)
                    g.setColor(java.awt.Color.BLACK);
                else
                    g.setColor(java.awt.Color.WHITE);
                g.fillRect(j*cellaSzlessg, i*cellaMagassg,
                        cellaSzlessg, cellaMagassg);
                // Rcs kirajzolsa
                g.setColor(java.awt.Color.LIGHT_GRAY);
                g.drawRect(j*cellaSzlessg, i*cellaMagassg,
                        cellaSzlessg, cellaMagassg);
            }
        }
        // Ksztnk pillanatfelvtelt?
        if(pillanatfelvtel) {
            // a biztonsg kedvrt egy kp ksztse utn
            // kikapcsoljuk a pillanatfelvtelt, hogy a
            // programmal ismerked Olvas ne rja tele a
            // fjlrendszert a pillanatfelvtelekkel
            pillanatfelvtel = false;
            pillanatfelvtel(robot.createScreenCapture
                    (new java.awt.Rectangle
                    (getLocation().x, getLocation().y,
                    szlessg*cellaSzlessg,
                    magassg*cellaMagassg)));
        }
    }
    /**
     * Az krdezett llapotban lv nyolcszomszdok szma.
     *
     * @param   rcs    a sejttr rcs
     * @param   sor     a rcs vizsglt sora
     * @param   oszlop  a rcs vizsglt oszlopa
     * @param   llapor a nyolcszomszdok vizsglt llapota
     * @return int a krdezett llapotbeli nyolcszomszdok szma.
     */
    public int szomszdokSzma(boolean [][] rcs,
            int sor, int oszlop, boolean llapot) {        
        int llapotSzomszd = 0;
        // A nyolcszomszdok vgigzongorzsa:
        for(int i=-1; i<2; ++i)
            for(int j=-1; j<2; ++j)
                // A vizsglt sejtet magt kihagyva:
                if(!((i==0) && (j==0))) {
            	// A sejttrbl szlnek szomszdai
            	// a szembe oldalakon ("peridikus hatrfelttel")
            	int o = oszlop + j;
         	   if(o < 0)
         	       o = szlessg-1;
         	   else if(o >= szlessg)
         	       o = 0;
         	   
         	   int s = sor + i;
         	   if(s < 0)
         	       s = magassg-1;
         	   else if(s >= magassg)
         	       s = 0;
         	   
         	   if(rcs[s][o] == llapot)
         	       ++llapotSzomszd;
                }
        
        return llapotSzomszd;
    }
    /**
     * A sejttr idbeli fejldse a John H. Conway fle
     * letjtk sejtautomata szablyai alapjn trtnik.
     * A szablyok rszletes ismertetst lsd pldul a
     * [MATEK JTK] hivatkozsban (Cskny Bla: Diszkrt
     * matematikai jtkok. Polygon, Szeged 1998. 171. oldal.)
     */
    public void idFejlds() {
        
        boolean [][] rcsEltte = rcsok[rcsIndex];
        boolean [][] rcsUtna = rcsok[(rcsIndex+1)%2];
        
        for(int i=0; i<rcsEltte.length; ++i) { // sorok
            for(int j=0; j<rcsEltte[0].length; ++j) { // oszlopok
                
                int lk = szomszdokSzma(rcsEltte, i, j, L);
                
                if(rcsEltte[i][j] == L) {
                /* l l marad, ha kett vagy hrom l
                 szomszedja van, klnben halott lesz. */
                    if(lk==2 || lk==3)
                        rcsUtna[i][j] = L;
                    else
                        rcsUtna[i][j] = HALOTT;
                }  else {
                /* Halott halott marad, ha hrom l
                 szomszedja van, klnben l lesz. */
                    if(lk==3)
                        rcsUtna[i][j] = L;
                    else
                        rcsUtna[i][j] = HALOTT;
                }
            }
        }
        rcsIndex = (rcsIndex+1)%2;
    }
    /** A sejttr idbeli fejldse. */
    public void run() {
        
        while(true) {
            try {
                Thread.sleep(vrakozs);
            } catch (InterruptedException e) {}
            
            idFejlds();
            repaint();
        }
    }
    /**
     * A sejttrbe "llnyeket" helyeznk, ez a "sikl".
     * Adott irnyban halad, msolja magt a sejttrben.
     * Az llny ismertetst lsd pldul a
     * [MATEK JTK] hivatkozsban (Cskny Bla: Diszkrt
     * matematikai jtkok. Polygon, Szeged 1998. 172. oldal.)
     *
     * @param   rcs    a sejttr ahov ezt az llatkt helyezzk
     * @param   x       a befoglal tgla bal fels sarknak oszlopa
     * @param   y       a befoglal tgla bal fels sarknak sora
     */
    public void sikl(boolean [][] rcs, int x, int y) {
        
        rcs[y+ 0][x+ 2] = L;
        rcs[y+ 1][x+ 1] = L;
        rcs[y+ 2][x+ 1] = L;
        rcs[y+ 2][x+ 2] = L;
        rcs[y+ 2][x+ 3] = L;
        
    }
    /**
     * A sejttrbe "llnyeket" helyeznk, ez a "sikl gy".
     * Adott irnyban siklkat l ki.
     * Az llny ismertetst lsd pldul a
     * [MATEK JTK] hivatkozsban /Cskny Bla: Diszkrt
     * matematikai jtkok. Polygon, Szeged 1998. 173. oldal./,
     * de itt az bra hibs, egy oszloppal told mg balra a 
     * bal oldali 4 sejtes ngyzetet. A helyes gy rajzt 
     * lsd pl. az [LET CIKK] hivatkozsban /Robert T. 
     * Wainwright: Life is Universal./ (Megemlthetjk, hogy
     * mindkett tartalmaz kt felesleges sejtet is.)
     *
     * @param   rcs    a sejttr ahov ezt az llatkt helyezzk
     * @param   x       a befoglal tgla bal fels sarknak oszlopa
     * @param   y       a befoglal tgla bal fels sarknak sora
     */    
    public void siklKilv(boolean [][] rcs, int x, int y) {
        
        rcs[y+ 6][x+ 0] = L;
        rcs[y+ 6][x+ 1] = L;
        rcs[y+ 7][x+ 0] = L;
        rcs[y+ 7][x+ 1] = L;
        
        rcs[y+ 3][x+ 13] = L;
        
        rcs[y+ 4][x+ 12] = L;
        rcs[y+ 4][x+ 14] = L;
        
        rcs[y+ 5][x+ 11] = L;
        rcs[y+ 5][x+ 15] = L;
        rcs[y+ 5][x+ 16] = L;
        rcs[y+ 5][x+ 25] = L;
        
        rcs[y+ 6][x+ 11] = L;
        rcs[y+ 6][x+ 15] = L;
        rcs[y+ 6][x+ 16] = L;
        rcs[y+ 6][x+ 22] = L;
        rcs[y+ 6][x+ 23] = L;
        rcs[y+ 6][x+ 24] = L;
        rcs[y+ 6][x+ 25] = L;
        
        rcs[y+ 7][x+ 11] = L;
        rcs[y+ 7][x+ 15] = L;
        rcs[y+ 7][x+ 16] = L;
        rcs[y+ 7][x+ 21] = L;
        rcs[y+ 7][x+ 22] = L;
        rcs[y+ 7][x+ 23] = L;
        rcs[y+ 7][x+ 24] = L;
        
        rcs[y+ 8][x+ 12] = L;
        rcs[y+ 8][x+ 14] = L;
        rcs[y+ 8][x+ 21] = L;
        rcs[y+ 8][x+ 24] = L;
        rcs[y+ 8][x+ 34] = L;
        rcs[y+ 8][x+ 35] = L;
        
        rcs[y+ 9][x+ 13] = L;
        rcs[y+ 9][x+ 21] = L;
        rcs[y+ 9][x+ 22] = L;
        rcs[y+ 9][x+ 23] = L;
        rcs[y+ 9][x+ 24] = L;
        rcs[y+ 9][x+ 34] = L;
        rcs[y+ 9][x+ 35] = L;
        
        rcs[y+ 10][x+ 22] = L;
        rcs[y+ 10][x+ 23] = L;
        rcs[y+ 10][x+ 24] = L;
        rcs[y+ 10][x+ 25] = L;
        
        rcs[y+ 11][x+ 25] = L;
        
    }
    /** Pillanatfelvtelek ksztse. */
    public void pillanatfelvtel(java.awt.image.BufferedImage felvetel) {
        // A pillanatfelvtel kp fjlneve
        StringBuffer sb = new StringBuffer();
        sb = sb.delete(0, sb.length());
        sb.append("sejtautomata");
        sb.append(++pillanatfelvtelSzmll);
        sb.append(".png");
        // png formtum kpet mentnk
        try {
            javax.imageio.ImageIO.write(felvetel, "png",
                    new java.io.File(sb.toString()));
        } catch(java.io.IOException e) {
            e.printStackTrace();
        }
    }
    // Ne villogjon a fellet (mert a "gyri" update()
    // lemeszeln a vszon fellett).    
    public void update(java.awt.Graphics g) {
        paint(g);
    }    
    /**
     * Pldnyost egy Conway-fle letjtk szablyos
     * sejttr obektumot.
     */    
    public static void main(String[] args) {
        // 100 oszlop, 75 sor mrettel:
        new Sejtautomata(100, 75);
    }
}

