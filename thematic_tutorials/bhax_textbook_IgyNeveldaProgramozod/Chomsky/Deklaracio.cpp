#include <stdio.h>

int *f(int* a, int *b){         //egészre mutató mutatót visszaadó függvény
    if (*a>=*b)
        return a;
    return b;
}

int h(int a, int b){         //egészre mutató mutatót visszaadó függvény
    if (a>=b)
        return a;
    return b;
}

int sum(int a, int b){
    return a+b;
}

int (*g (int n)) (int a,int b){ //egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó, egészet kapó függvény
    if(n%2==0)
        return sum;
}

int main(){
    int i=1; //egész
    int *a=&i; //egészre mutató mutató
    int &r=i;//egész referenciája
    int tomb[]={1,2,3,4}; //egészek tömbje
    int (&tr)[4]=tomb;//egészek tömbjének referenciája
    int* tomb2[]={tomb,tomb+1,tomb+2,tomb+3};//egészre mutató mutatók tömbje
    int *(*p)(int*,int*); //egészre mutató mutatót visszaadó függvényre mutató mutató
    p=f;
    int (*(*pp) (int))(int,int);  //függvénymutató egy egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó, egészet kapó függvényre
    int (*k)(int,int);
    k=h;
    pp=g;
    k=*pp(2);
    printf("%x\n",h(i,tomb[1]));

}