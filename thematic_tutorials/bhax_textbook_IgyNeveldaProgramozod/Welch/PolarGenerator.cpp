#include <iostream>
#include <math.h>
#include <random>
#include <functional>
#include <chrono>

class Unirand {

    private:
        std::function <int()> random;

    public:
        Unirand(long seed, int min, int max): random(
            std::bind(
                std::uniform_int_distribution<>(min, max),
                std::default_random_engine(seed) 
            )
        ){}    

   int operator()(){return random();}
        
};
Unirand ur{std::chrono::system_clock::now().time_since_epoch().count(), 0, 1000000};
double whereToPut() {
        
            return ur()/1000000.0;
    }

class generator{

    
    bool nincstarolt = true;
    double tarolt;

    public:
    generator(){this->nincstarolt=true;}
    double kovetkezo(){
        if(nincstarolt) {
            
            double u1, u2, v1, v2, w;
            do {
                
                u1 = whereToPut();
                u2 = whereToPut();
                
                v1 = 2*u1 - 1;
                v2 = 2*u2 - 1;
                
                w = v1*v1 + v2*v2;
                
            } while(w > 1);
            double r = sqrt((-2*log(w))/w);
            
            tarolt = r*v2;
            nincstarolt = !nincstarolt;
            
            return r*v1;
            
        } else {
            nincstarolt = !nincstarolt;
            return tarolt;
        }
    }
};

int main(){
    generator g;
    for (int i = 0; i < 10; i++)
    {
        std::cout<<g.kovetkezo()<<std::endl;
    }
    
}